$(document).ready(function() {

    $('.add-to-cart').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: 'Cart/AddToCart',
            type: 'POST',
            dataType: 'text',
            data: {
                'product_id': $(this).data("id")
            },
            success: function(response) {
                if (response == 1 ) {
                    $.ajax({
                        url: 'Cart/getQuantityInCart',
                        method: 'POST',
                        data : {
                            type: 'ajax'
                        },
                        success: function(data) {
                            $('#cart-result').html(data);
                        }
                    });
                } else {
                    alert('Add to cart failed')
                }
            }
        });
    });

    $('.buy-now').click(function() {
        $('.add-to-cart').trigger('click');
        window.location.href = 'Cart';
    })

    $('a.remove-product').click(function(e) {
        e.preventDefault();
        var cart_id = $(this).data('cart_id');
        $.ajax({
            url: 'Cart/removeProductInCart',
            type: 'POST',
            data: { 
                'cart_id': cart_id 
            },
            success: function(response) {
                // alert('Removed success');
                location.reload();
            }
        });
    });
});

