function checkInfo() {
    var isSubmit = true;
    var firstName = document.getElementById('firstName').value;
    if (firstName.length == '') {
        document.getElementById('error_firstName').style.display = 'block';
        isSubmit = false;
    } else {
        document.getElementById('error_firstName').style.display = 'none';
    }

    var lastName = document.getElementById('lastName').value;
    if (lastName.length == '') {
        document.getElementById('error_lastName').style.display = 'block';
        isSubmit = false;
    } else {
        document.getElementById('error_lastName').style.display = 'none';
    }
    
    var address = document.getElementById('address').value;
    if (address.length == '') {
        document.getElementById('error_address').style.display = 'block';
        isSubmit = false;
    } else {
        document.getElementById('error_address').style.display = 'none';
    }

    var phone = document.getElementById('phone').value;
    if (phone.length == '') {
        document.getElementById('error_phone').style.display = 'block';
        isSubmit = false;
    } else {
        document.getElementById('error_phone').style.display = 'none';
    }

    var email = document.getElementById('email').value;
    if (email.length == '') {
        document.getElementById('error_email').style.display = 'block';
        isSubmit = false;
    } else {
        document.getElementById('error_email').style.display = 'none';
    }
    
    var isChecked = document.getElementById("ship_to_different_address").checked;
    if (isChecked) {
        var firstNameShipping = document.getElementById('firstNameShipping').value;
        if (firstNameShipping.length == '') {
            document.getElementById('error_firstName_shipping').style.display = 'block';
            isSubmit = false;
        } else {
            document.getElementById('error_firstName_shipping').style.display = 'none';
        }

        var lastNameShipping = document.getElementById('lastNameShipping').value;
        if (lastNameShipping.length == '') {
            document.getElementById('error_lastName_shipping').style.display = 'block';
            isSubmit = false;
        } else {
            document.getElementById('error_lastName_shipping').style.display = 'none';
        }
        
        var addressShipping = document.getElementById('address').value;
        if (addressShipping.length == '') {
            document.getElementById('error_address_shipping').style.display = 'block';
            isSubmit = false;
        } else {
            document.getElementById('error_address_shipping').style.display = 'none';
        }

        var phoneShipping = document.getElementById('phoneShipping').value;
        if (phoneShipping.length == '') {
            document.getElementById('error_phone_shipping').style.display = 'block';
            isSubmit = false;
        } else {
            document.getElementById('error_phone_shipping').style.display = 'none';
        }

        var emailShipping = document.getElementById('email').value;
        if (emailShipping.length == '') {
            document.getElementById('error_email_shipping').style.display = 'block';
            isSubmit = false;
        } else {
            document.getElementById('error_email_shipping').style.display = 'none';
        }
    }
    var termsAndCondition = document.querySelector('.termsAndCondition:checked');
    if (termsAndCondition == null) {
        isSubmit = false;
        alert('Please accept the terms and conditions');
    }
    if (isSubmit) {
        return true;
    }

    return false;
    
}

function shipToDifferentAddress() {
    var ship_to_different_address = document.getElementById("ship_to_different_address");
    var shipping_address = document.getElementById("shipping_address");
    shipping_address.style.display = ship_to_different_address.checked ? "block" : "none";
}


function checkThongtin() {

    console.log(document.getElementById('firstName').value)
    if (document.getElementById('firstName').value.length > 0) {
        document.getElementById('error_firstName').style.display = 'none';
    }

}