<?php

class Cart extends BaseController {

    private $categoryModel;
    public function __construct() {
        $this->categoryModel = $this->model('CategoryModel');
    }

    public function index() {
        $categories = $this->categoryModel->getCategory(0);

        $product = $this->model('ProductModel');
        $productByCategory = $product->getProductsByCategory(8);

        $quantity = $this->getQuantityInCart();

        $productInCart = $this->getProductInCart();
        
        $this->view('layouts/shop', [
            "manHinh" => "cart",
            "categories" => $categories,
            'products' => $productByCategory,
            'quantity' => $quantity,
            'productInCart' => $productInCart
        ]);
    }

    public function getSubCategory($id) {
        $subCategory = $this->categoryModel->getCategory($id);
        return $subCategory;
    }


    public function AddToCart() {
        $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : '';
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = rand();
        }
        $cartModel = $this->model('CartModel');
        $productInCart = $cartModel->checkProductInCart($_SESSION['cart'], $product_id);
        $currentQuantity = 1;
        if (!empty($productInCart)) {
            $currentQuantity = $productInCart->quantity;
            $currentQuantity = $currentQuantity + 1;
            $cartModel->removeCart($productInCart->id);
        }
        $result = $cartModel->addToCart($product_id, $currentQuantity, $_SESSION['cart']);
        echo $result;
    }

    public function getProductInCart() {
        $session_cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : '';
        $cartModel = $this->model('CartModel');
        $productModel = $this->model('ProductModel');
        $cartShow = $cartModel->getCartBySessionCart($session_cart);
        $arrayProductInCart = array();
        foreach ($cartShow as $cart) {
            $product_id = $cart->product_id;
            $product = $productModel->getProductInCartById($product_id);
            array_push($arrayProductInCart, $product);
        }
        $_SESSION['allProductsInCart'] = $arrayProductInCart;
        
        return $arrayProductInCart;
    }

    public function getQuantityInCart() {
        if (isset($_SESSION['cart'])) {
            $cartModel = $this->model('CartModel');
            $quantity = $cartModel->getQuantity($_SESSION['cart']);
            if (isset($_POST['type'])) {
                echo $quantity;
                return;
            }
            return $quantity;
        }
    }

    public function removeProductInCart() {
        $cart_id = isset($_POST['cart_id']) ? $_POST['cart_id'] : '';
        if (!empty($cart_id)) {
            //remove product in cart
            $cartModel = $this->model('CartModel');
            $result = $cartModel->removeCart($cart_id);
            echo $result;
        } else {
            echo 'Error';
        }
    }

}