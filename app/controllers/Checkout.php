<?php

class Checkout extends BaseController {
    
    private $categoryModel;
    public function __construct() {
        $this->categoryModel = $this->model('CategoryModel');
    }

    public function index() {
        if (!isset($_SESSION['user'])) {
            $loginPage = base_url('Customer');
            header("Location: $loginPage");
            return;
        }

        // Call model to get all categories
        $categories = $this->categoryModel->getCategory(0);

        //Call controller to get current quantity
        $cartController = $this->controller('Cart');
        $quantity = $cartController->getQuantityInCart();

        // Call model to get all payment methods
        $paymentMethodsModels = $this->model('PaymentMethodModel');
        $paymentMethods = $paymentMethodsModels->getPaymentMethods();

        $this->view('pages/checkout', [
            "categories" => $categories,
            'quantity' => $quantity,
            'paymentMethods' => $paymentMethods
        ]);
    }

    public function getSubCategory($id) {
        $subCategory = $this->categoryModel->getCategory($id);
        return $subCategory;
    }

    public function handleBillingAddress() {
        if (!isset($_SESSION['user'])) {
            $loginPage = base_url('Customer');
            header("Location: $loginPage");
            return;
        }
        if (isset($_POST['placeOrder'])) {
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $country_id = $_POST['country_id'];
            $address = $_POST['address'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $_SESSION['payment_method_id'] = $_POST['rdPaymentMethod'];
            $customer = $_SESSION['user'];

            if (!empty($firstName) && !empty($lastName) && !empty($address) && !empty($phone) && !empty($email)) {
                // insert table billing
                $billingModel = $this->model('BillingModel');

                $result = $billingModel->insertBillingAddress($firstName, $lastName, $country_id, $address, $phone, $email, $customer->id);
                if ($result > 0) {
                    if (isset($_POST['shippingToAddressOther']) && $_POST['shippingToAddressOther'] == 1) {
                        //insert table shipping
                        if (!empty($_POST['firstNameShipping']) && !empty($_POST['lastNameShipping']) && !empty($_POST['addressShipping']) && !empty($_POST['phoneShipping']) && !empty($_POST['emailShipping'])) {
                            //Call to ShippingModel
                            $shippingModel = $this->model('ShippingModel');
                            //Insert to table shipping
                            $result = $shippingModel->addShippingAddress($_POST['firstNameShipping'], $_POST['lastNameShipping'], $_POST['country_id_shipping'], $_POST['addressShipping'], $_POST['phoneShipping'], $_POST['emailShipping'], $customer->id);
                            if ($result == 0) {
                                $billingModel->rollbackTransaction();
                                return header("Location: http://localhost:8080/Ecommerce_OOP/Checkout");
                            }
                        } else {

                        }
                        
                    }
                    
                    //insert vao table order
                    $customer = $_SESSION['user'];
                    $this->createNewOrder($customer);
                    // delete session when create order success
                    unset($_SESSION['allProductsInCart']);
                    // Remove cart by session cart
                    $cartModel = $this->model('CartModel');
                    $result = $cartModel->removeCartBySessionCart();
                    unset($_SESSION['cart']);
                    unset($_SESSION["shippingFee"]);
                    unset($_SESSION['discount']);
                    unset($_SESSION['totalPrice']);
                    unset($_SESSION['subTotalPrice']);
                    
                    return header("Location: http://localhost/ecommerce_oop/Customer/MyAccount");
                } else {
                    
                }
                
            } else {

            }
        }
        
        
    }

    public function createNewOrder($customer) {
        $allProductsInCart = isset($_SESSION['allProductsInCart']) ? $_SESSION['allProductsInCart'] : '';

        if (!empty($allProductsInCart)) {
            $orderModel = $this->model('OrderModel');
            $orderModel->insertOrder($allProductsInCart, $customer);
        } else {
            //No product in cart
            return header("Location: http://localhost:8080/Ecommerce_OOP");
        }
        
    }

}


?>