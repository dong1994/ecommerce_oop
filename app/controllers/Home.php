<?php

class Home extends BaseController {
    
    private $categoryModel;
    public function __construct() {
        $this->categoryModel = $this->model('CategoryModel');
    }

    public function index() {
        $categories = $this->categoryModel->getCategory(0);

        $slide = $this->model('SlideModel');
        $slides = $slide->getSlides();

        $product = $this->model('ProductModel');
        $productByCategory = $product->getProductsByCategory(8);

        $cartController = $this->controller('Cart');
        $quantity = $cartController->getQuantityInCart();
      
        $this->view('layouts/master', [
            "page" => "index",
            "categories" => $categories,
            'slides' => $slides,
            'products' => $productByCategory,
            'quantity' => $quantity
        ]);
    }

    public function getSubCategory($id) {
        $subCategory = $this->categoryModel->getCategory($id);
        return $subCategory;
    }


}


?>