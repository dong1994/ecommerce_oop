<?php

class Customer extends BaseController {

    private $customerModel;
    public function __construct() {
        $this->customerModel = $this->model('CustomerModel');
    }
    
    public function index() {
        
        $this->view('pages/login', [
            "manHinh" => "login",
        ]);
    }

    public function sign_up() {
        $this->view('pages/register', [
            "manHinh" => "register",
        ]);
    }

    public function registerAccount() {
        if (isset($_POST['btnRegister'])) {
            $user = $this->customerModel->getCustomerByEmail($_POST['email']);
            if ($user == false) {
                $password = md5(trim($_POST['password']));
                $newCustomer = $this->customerModel->createNewCustomer($_POST['name'], $_POST['email'], $password);
                if ($newCustomer > 0) {
                    $user = $this->customerModel->getCustomerByEmail($_POST['email']);
                    $_SESSION['user'] = $user;
                    $this->signIn();
                } else {
                    echo 'Error. Cannot insert';
                    return;
                }
            } else {
                // Email da ton tai
            }
            
        }
    }


    public function signIn() {
        if(isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
            $email = $user->email;
            $password = $user->password;
             $this->customerModel->login($email, $password);
            $homePage = base_url('/');
            header("Location: $homePage");
        } else {
            if(isset($_POST['btnLogin'])) {
                $email = isset($_POST['email']) ? $_POST['email'] : '';
                $password = isset($_POST['password']) ? md5(trim($_POST['password'])) : '';
                if (!empty($email) && !empty($password)) {
                    $user = $this->customerModel->login($email, $password);
                    if ($user != false) {
                        $_SESSION['user'] = $user;
                        $homePage = base_url('');
                        header("Location: $homePage");
                    } else {
                        // email va password ko ton tai
                        $_SESSION['error'] = 'Email or password is incorrect. Please check!';
                        $loginPage = base_url('Customer');
                        header("Location: $loginPage");
                    }
                } else {
                    $_SESSION['error'] = 'Please enter email and password!';
                    $loginPage = base_url('Customer');
                    header("Location: $loginPage");
                }
                
            }
        }
    }

    public function Logout() {
        unset($_SESSION['user']);
        $loginPage = base_url('');
        header("Location: $loginPage");
    }

    public function MyAccount() {
        if (!isset($_SESSION['user'])) {
            $loginPage = base_url('Customer');
            header("Location: $loginPage");
            return;
        }
        $orderModel = $this->model('OrderModel');
        $historyOrder = $orderModel->History();

        $this->view('pages/my_account', [
            'historyOrder' => $historyOrder
        ]);
    }

    public function OrderDetail($order_id) {
        $orderModel = $this->model('OrderModel');
        $orderDetail = $orderModel->getOrderDetail($order_id);
        print_r(json_encode($orderDetail));
    }

}


?>