<?php

class Shop extends BaseController {
    
    private $categoryModel;
    public function __construct() {
        $this->categoryModel = $this->model('CategoryModel');
    }

    public function index() {
        $categories = $this->categoryModel->getCategory(0);

        $product = $this->model('ProductModel');
        $productByCategory = $product->getProductsByCategory(8);

        $cartController = $this->controller('Cart');
        $quantity = $cartController->getQuantityInCart();
        
        $this->view('layouts/shop', [
            "manHinh" => "product",
            "categories" => $categories,
            'products' => $productByCategory,
            'quantity' => $quantity
        ]);
    }

    public function getSubCategory($id) {
        $subCategory = $this->categoryModel->getCategory($id);
        return $subCategory;
    }

}


?>