<?php

class Application {
    
    private $controller = "Home";
    private $action = "index";
    private $params = [];

    public function __construct()
    {
        $arr_url = $this->url_proccess();
        // var_dump($arr_url);
        
        // Xu ly controller
        if (file_exists('./app/controllers/'.$arr_url[0].'.php')) { //file_exists => true/false
            $this->controller = $arr_url[0];
            unset($arr_url[0]);
        }
        require_once("./app/controllers/".$this->controller.'.php');
        $this->controller = new $this->controller;

        // Xu ly action
        if (isset($arr_url[1])) {
            if (method_exists($this->controller, $arr_url[1])) {
                $this->action = $arr_url[1];
            }
            unset($arr_url[1]);
        }

        //Xu ly params
        $this->params = $arr_url ? array_values($arr_url) : [];
        // var_dump($this->params) ;
        // die;
        call_user_func_array([$this->controller, $this->action], $this->params);

    }

    private function url_proccess() {
        if (isset($_GET['url'])) {
            $url = trim($_GET['url']);
            $arr_url = explode("/", $url);
            return $arr_url;
        }
    }
}


?>