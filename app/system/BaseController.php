<?php

class BaseController {
    
    public function model($model) {
        require_once('./app/models/'.$model.'.php');
        return new $model;
    }

    public function view($view, $data = []) {
        if(!file_exists('./app/views/'.$view.'.php')) {
            $view = 'errors/404';
        }
        require_once('./app/views/'.$view.'.php');
    }

    public function controller($controller) {
        require_once('./app/controllers/'.$controller.'.php');
        return new $controller;
    }
}


?>