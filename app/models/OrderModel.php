<?php

class OrderModel extends Connection {
    
    public function insertOrder($allProductsInCart, $customer) {
        
        try {
            $this->_dbh->beginTransaction();
            $currentDay = date('Y-m-d H:i:s');
            $totalPrice = $_SESSION['totalPrice'];
            $paymentMethodId = $_SESSION['payment_method_id'];
            unset($_SESSION['payment_method_id']);

            $sqlOrder = "INSERT INTO orders(created_order, customer_name, customer_email, total_amount, order_status, payment_method_id)
            VALUES('$currentDay', '$customer->name', '$customer->email', '$totalPrice', 'Created', $paymentMethodId)";
            $this->setQuery($sqlOrder);
            $resultOrder = $this->execute();
            $this->_dbh->commit();
            
            $sql = "SELECT * FROM orders WHERE created_order = '$currentDay'";
            $this->setQuery($sql);
            $orderInserted = $this->loadRow();
            foreach($allProductsInCart as $product) {
                $sqlOrderDetail = "INSERT into order_detail(order_id, product_id, product_name, unit_price, quantity)
                VALUES ('$orderInserted->id', '$product->id', '$product->name', '$product->price', '$product->quantity')";
                $this->setQuery($sqlOrderDetail);
                $resultOrderDetail = $this->execute();
                $resultOrderDetail->rowCount();
            }

        } catch (Exception $e) {
            $this->_dbh->rollBack();
        }
        
    }

    public function History() {
        $email = $_SESSION['user']->email;
        $sql ="SELECT orders.*, payment_method.name as PaymentName FROM orders JOIN payment_method ON orders.payment_method_id = payment_method.id WHERE customer_email = '$email' ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function getOrderDetail($order_id) {
        $sql = "SELECT * FROM order_detail WHERE order_id = '$order_id'";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}

?>