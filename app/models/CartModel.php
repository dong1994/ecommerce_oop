<?php

class CartModel extends Connection {
    
    public function addToCart($product_id, $quantity, $sesison_cart) {
        $sql = "INSERT INTO cart(product_id, quantity, session_cart) VALUES($product_id, $quantity, $sesison_cart)";
        $this->setQuery($sql);
        $result = $this->execute();
        return $result->rowCount();
    }

    public function getQuantity($sesison_cart) {
        $sql = "SELECT SUM(quantity)  FROM cart WHERE session_cart = $sesison_cart";
        $this->setQuery($sql);
        $result = $this->loadRecord();
        return $result;
    }

    public function getCartBySessionCart($cart_session) {
        $sql = "SELECT * FROM cart WHERE session_cart = $cart_session";
        $this->setQuery($sql);
        $result = $this->loadAllRows();
        return $result;
    }
   
    public function removeCart($cart_id) {
        $sql = "DELETE FROM cart WHERE id = $cart_id";
        $this->setQuery($sql);
        $result = $this->execute();
        return $result->rowCount();
    }
    
    public function checkProductInCart($sesison_cart, $product_id) {
        $sql = "SELECT * FROM cart WHERE session_cart = $sesison_cart AND product_id = $product_id";
        $this->setQuery($sql);
        $result = $this->loadRow();
        return $result;
    }

    public function removeCartBySessionCart() {
        $session_cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : '';
        if(!empty($session_cart)) {
            $sql = "DELETE FROM cart WHERE session_cart = $session_cart";
            $this->setQuery($sql);
            $result = $this->execute();
            return $result->rowCount();
        }
    }
    
}

?>