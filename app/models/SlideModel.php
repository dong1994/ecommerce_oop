<?php

class SlideModel extends Connection {
    
    public function getSlides() {
        $sql = "SELECT * FROM slide WHERE status = 1";
        $this->setQuery($sql);
        $slides = $this->loadAllRows();
        return $slides;
    }
}

?>