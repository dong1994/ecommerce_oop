<?php

class PaymentMethodModel extends Connection {
    
    public function getPaymentMethods() {
        $sql = "SELECT * FROM payment_method WHERE status = 1";
        $this->setQuery($sql);
        $paymentMethods = $this->loadAllRows();
        return $paymentMethods;
    }
}

?>