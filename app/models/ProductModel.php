<?php

class ProductModel extends Connection {
    
    public function getProductsByCategory($category_id) {
        $sql = "SELECT * FROM products WHERE category_id = $category_id AND status = 1 LIMIT 0, 4";
        $this->setQuery($sql);
        $products = $this->loadAllRows();
        return $products;
    }

    public function getProductById($product_id) {
        $sql = "SELECT * FROM products WHERE id = $product_id";
        $this->setQuery($sql);
        $products = $this->loadRow();
        return $products;
    }

    public function getProductInCartById($product_id) {
        //$sql = "SELECT * FROM products WHERE id = $product_id JOIN cart ON product.id = cart.product_id";
        $sql = "SELECT products.*, cart.quantity, cart.id AS cart_id FROM products  JOIN cart ON products.id = cart.product_id WHERE products.id = $product_id";
        $this->setQuery($sql);
        $products = $this->loadRow();
        return $products;
    }
}

?>