<section class="ftco-section ftco-cart">
	<div class="container">
		<?php if (count($data['productInCart']) > 0) {
			$subTotalPrice = 0;
		?>
		<div class="row">
			<div class="col-md-12 ftco-animate">
				<div class="cart-list">
					<table class="table">
						<thead class="thead-primary">
							<tr class="text-center">
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Product</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							</tr>
						</thead>
						<tbody>

						<?php foreach ($data['productInCart'] as $product_cart) {?>
							<tr class="text-center">
							<td class="product-remove"><a class="remove-product" data-cart_id=<?= $product_cart->cart_id?> href="#"><span class="ion-ios-close"></span></a></td>
							
							<td class="image-prod"><div class="img" style="background-image:url(./public/site/images/<?php echo $product_cart->image?>);"></div></td>
							
							<td class="product-name">
								<h3><?php echo $product_cart->name?></h3>
								<p><?php echo $product_cart->description?></p>
							</td>
							
							<td class="price">$<?php echo $product_cart->price?></td>
							
							<td class="quantity">
								<div class="input-group mb-3">
								<input type="text" name="quantity" class="quantity form-control input-number" value="<?php echo $product_cart->quantity?>" min="1" max="100">
							</div>
							</td>
							
							<td class="total">$<?php
												 $priceProduct = $product_cart->price * $product_cart->quantity;
												 echo $priceProduct;
												 $subTotalPrice  += $priceProduct;
												 ?></td>
							</tr><!-- END TR-->

						<?php }?>
							
						</tbody>
						</table>
					</div>
			</div>
		</div>
		<div class="row justify-content-start">
			<div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
				<div class="cart-total mb-3">
					<h3>Cart Totals</h3>
					<p class="d-flex">
						<span>Subtotal</span>
						<span>$<?php echo $subTotalPrice.'.00'; ?></span>
					</p>
					<p class="d-flex">
						<span>Shipping Fee</span>
						<span>
							<?php 
							$shippingFee = 3;
							if ($shippingFee > 0) {
								echo '$'.$shippingFee.'.00';
							} else {
								echo 'Free Shipping';
							}
							$_SESSION['shippingFee'] = $shippingFee;
							?>
						</span>
					</p>

					<?php
					   $discount = 10;
					   if ($discount > 0) {
					?>
						<p class="d-flex">
							<span>Discount</span>
							<span>-$
								<?php 
								echo $discount
								?>.00
							</span>
						</p>
					<?php }
					$_SESSION['discount'] = $discount;
					?>

					<hr>
					<p class="d-flex total-price">
						<span>Total</span>
						<span>$<?php 
							echo $totalPrice = $subTotalPrice + ($shippingFee > 0 ? $shippingFee : 0) - ($discount > 0 ? $discount : 0);
							$_SESSION['totalPrice'] = $totalPrice;
							$_SESSION['subTotalPrice'] = $subTotalPrice;
							
							 ?>.00</span>
					</p>
				</div>
				<p class="text-center"><a href="Checkout" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
			</div>
		</div>
		<?php } else { ?>
			<h1 style="text-align: center">Cart empty!. No products in cart</h1>
		<?php }?>
	</div>
</section>