<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Account</title>
    
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
    <style>
        #order_history {
            display: none
        }
    </style>
    <script>
        function handleTab() {
            let order_history = document.querySelector("#order_history");
            let profile = document.querySelector("#profile");
            if(document.querySelector("#order_history").style.display != "block") {
                document.querySelector("#order_history").style.display = "block";
                profile.style.display = "none";
            } else {
                document.querySelector("#order_history").style.display = "none";
                profile.style.display = "block";
            }
        }

        function callAjaxToGetOrderDetail(id) {
            $.ajax({
                url: "<?php echo base_url('Customer/OrderDetail/')?>" + id,
                success: function(response) {
                    response = JSON.parse(response);
                    var productName = '', price = '', quantity = '';
                    for(var i = 0; i < response.length; i++) {
                        var dash = ' - ';
                        if (i == response.length - 1) {
                            dash = '';
                        }
                        productName += response[i].product_name + dash;
                        price += response[i].unit_price + dash;
                        quantity += response[i].quantity + dash;
                        $('.modal-title').html(productName);
                        $('#unit_price').html(price);
                        $('#quantity_pr').html(quantity);
                    }
                }
            })
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2>MY ACCOUNT</h2>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" onclick="handleTab()">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="handleTab()">History</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" id="profile">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form action="">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" value="<?php echo $_SESSION['user']->name?>" id="email" name="name" placeholder="Enter name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" value="<?php echo  $_SESSION['user']->email ?>" id="email" name="email" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="email">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                            </div>
                            <div class="form-group">
                                <label for="email">Confirm password</label>
                                <input type="password" class="form-control" id="password" name="confirm_password" placeholder="Enter confirm password">
                            </div>
                            <button href="#!" class=" btn btn-success card-link">Update</button>
                            <a href="#!" class=" btn btn-danger card-link">Logout</a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" id="order_history">
            <div class="col-md-2"></div>
            <div class="col-md-8" >
                <h2>Order History</h2>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Order ID</th>
                            <th scope="col">Order Created</th>
                            <th scope="col">Total amount</th>
                            <th scope="col">Payment method</th>
                            <th scope="col">Order status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($data['historyOrder']) > 0) { 
                            $stt = 0;
                            foreach ($data['historyOrder'] as $order) {
                                $stt ++;
                        ?>
                        <tr>
                            <th scope="row"><?php echo $stt;?></th>
                            <td><a href="#" onclick="callAjaxToGetOrderDetail(<?php echo $order->id?>)" data-toggle="modal" data-target="#myModal" data-order_id="<?php echo $order->id?>"><?php echo $order->id?></a></td>
                            <td><?php echo $order->created_order?></td>
                            <td><?php echo $order->total_amount?></td>
                            <td><?php echo $order->PaymentName?></td>
                            <td><?php echo $order->order_status?></td>
                        </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p id="unit_price">Some text in the modal.</p>
                </div>
                <div class="modal-quantity">
                    <p id="quantity_pr">Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            
            </div>
        </div>

    </div>
</body>
</html>