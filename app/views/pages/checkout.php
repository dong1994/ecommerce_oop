<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Minishop - Checkout</title>

    <?php require_once('./app/views/layouts/head_script.php')?>

  </head>
  <body class="goto-here">

    <?php require_once('./app/views/layouts/top.php')?>

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <?php require_once('./app/views/layouts/menu.php')?>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('./public/site/images/slides/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Checkout</span></p>
            <h1 class="mb-0 bread">Checkout</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
	  <form action="Checkout/handleBillingAddress" class="billing-form" method="post">
        <div class="row justify-content-center">
          <div class="col-xl-10 ftco-animate">
			
				<h3 class="mb-4 billing-heading">Billing Address</h3>
	          	<div class="row align-items-end">
	          		<div class="col-md-6">
	                <div class="form-group">
	                  <label for="firstname">Firt Name</label>
	                  <input type="text" class="form-control" onkeyup="return checkThongtin()" id="firstName" placeholder="Enter firstname" name="firstName">
					  <p style="color: red; display: none;" id="error_firstName">First name cannot be empty.</p>
					</div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="lastname">Last Name</label>
	                  <input type="text" class="form-control" id="lastName" placeholder="Enter lastname" name="lastName">
					  <p style="color: red; display: none;" id="error_lastName">Last name cannot be empty.</p>
					</div>
                </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">Country</label>
		            		<div class="select-wrap">
		                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
		                  <select id="" class="form-control" name="country_id">
						    <option value="1">Viet nam</option>
		                  </select>
		                </div>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
	                	<label for="streetaddress">Address</label>
	                  <input type="text" class="form-control" id="address" name="address" placeholder="Enter your address">
					  <p style="color: red; display: none;" id="error_address">Address cannot be empty.</p>
					</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Phone</label>
					  <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" minlength="10" maxlength="11">
					  <p style="color: red; display: none;" id="error_phone">Phone cannot be empty.</p>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="emailaddress">Email Address</label>
					  <input type="text" class="form-control" id="email" name="email" placeholder="Enter email">
					  <p style="color: red; display: none;" id="error_email">Email cannot be empty.</p>
	                </div>
                </div>
	            </div>
			  
			  <div class="w-100"></div>
                <div class="col-md-12">
                	<div class="form-group mt-4">
						<div class="radio">
							<label>
							<input type="checkbox" name="shippingToAddressOther" value="1" id="ship_to_different_address" onclick="shipToDifferentAddress()"> Ship to different address</label>
						</div>
					</div>
				</div>
				
		  </div> <!-- .col-md-8 -->

		  <!--Form shipping address-->
		<div class="row justify-content-center" id="shipping_address" style="display: none">
          <div class="col-xl-10 ftco-animate">
				<h3 class="mb-4 billing-heading">Shipping Address</h3>
	          	<div class="row align-items-end">
	          		<div class="col-md-6">
	                <div class="form-group">
	                  <label for="firstname">Firt Name</label>
	                  <input type="text" class="form-control" onkeyup="return checkThongtin()" id="firstNameShipping" placeholder="Enter firstname" name="firstNameShipping">
					  <p style="color: red; display: none;" id="error_firstName_shipping">First name cannot be empty.</p>
					</div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="lastname">Last Name</label>
	                  <input type="text" class="form-control" id="lastNameShipping" placeholder="Enter lastname" name="lastNameShipping">
					  <p style="color: red; display: none;" id="error_lastName_shipping">Last name cannot be empty.</p>
					</div>
                </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">Country</label>
		            		<div class="select-wrap">
		                  <select id="" class="form-control" name="country_id_shipping">
						    <option value="1">Viet nam</option>
		                  </select>
		                </div>
		            	</div>
		            </div>
		        <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
							<label for="streetaddress">Address</label>
							<input type="text" class="form-control" id="addressShipping" name="addressShipping" placeholder="Enter your address">
							<p style="color: red; display: none;" id="error_address_shipping">Address cannot be empty.</p>
						</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Phone</label>
					  <input type="text" class="form-control" id="phoneShipping" name="phoneShipping" placeholder="Enter phone" minlength="10" maxlength="11">
					  <p style="color: red; display: none;" id="error_phone_shipping">Phone cannot be empty.</p>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="emailaddress">Email Address</label>
					  <input type="text" class="form-control" id="emailShipping" name="emailShipping" placeholder="Enter email">
					  <p style="color: red; display: none;" id="error_email_shipping">Email cannot be empty.</p>
	                </div>
				</div>
				<div class="row mt-5 pt-3 d-flex">
	          	
			  </div>

	            </div>
		  </div> <!-- .col-md-8 -->

		  
		</div>
		
		<div class="col-md-6 d-flex">
			<div class="cart-detail cart-total bg-light p-3 p-md-4">
				<h3 class="billing-heading mb-4">Cart Total</h3>
				<p class="d-flex">
					<span>Subtotal</span>
					<span>$<?php echo $_SESSION['subTotalPrice']?>.0</span>
				</p>
				<p class="d-flex">
					<span>Delivery</span>
					<span>$<?php echo $_SESSION['shippingFee']?>.00</span>
				</p>
				<p class="d-flex">
					<span>Discount</span>
					<span>-$<?php echo $_SESSION['discount'] ?>.00</span>
				</p>
				<hr>
				<p class="d-flex total-price">
					<span>Total</span>
					<span>$<?php echo $_SESSION['totalPrice']?>.0</span>
				</p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="cart-detail bg-light p-3 p-md-4">
				<h3 class="billing-heading mb-4">Payment Method</h3>
					
				<?php foreach ($data['paymentMethods'] as $paymentMethod) { ?>
					<div class="form-group">
						<div class="col-md-12">
							<div class="radio">
								<label><input type="radio" checked name="rdPaymentMethod" class="mr-2" value="<?php echo $paymentMethod->id?>"> <?php echo $paymentMethod->name ?></label>
							</div>
						</div>
					</div>
				<?php }?>
				<div class="form-group">
					<div class="col-md-12">
						<div class="checkbox">
							<label><input type="checkbox" value="1" id="termsAndCondition" class="mr-2 termsAndCondition"> I have read and accept the terms and conditions</label>
						</div>
					</div>
				</div>
				<p><button onclick="return checkInfo()" type="submit" name="placeOrder" class="btn btn-primary py-3 px-4">Place an order</button></p>
			</div>
			</div>
		</div>
		</form><!-- END -->
      </div>
    </section> <!-- .section -->

    <?php require_once('./app/views/layouts/footer.php')?>

  <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


    <?php require_once('./app/views/layouts/foot_script.php')?>

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>