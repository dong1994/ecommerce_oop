<div class="container">
    <a class="navbar-brand" href="index.html">Minishop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item active"><a href="index.html" class="nav-link">Home</a></li>
        <li class="nav-item"><a href="Shop" class="nav-link">Shop</a></li>
        <?php 
            foreach($data['categories'] as $cate) {
                $subCategory = $this->getSubCategory($cate->id);
        ?>
        <li class="nav-item <?php if (count($subCategory) > 0) { ?>dropdown <?php } ?>">
            <a class="nav-link <?php if (count($subCategory) > 0) { ?>dropdown-toggle <?php } ?>" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $cate->name ?></a>
            <div class="dropdown-menu" aria-labelledby="dropdown04">
                <?php foreach($subCategory as $subCate) { ?>
                    <a class="dropdown-item" href="shop.html"><?php echo $subCate->name?></a>
                <?php } ?>
            </div>
        </li>
        <?php } ?>
        
        <?php if (!isset($_SESSION['user'])) { ?>
            <li class="nav-item"><a href="<?php echo base_url('Customer'); ?>" class="nav-link">Login</a></li>
            <li class="nav-item"><a href="<?php echo base_url('Customer/sign_up'); ?>" class="nav-link">Register</a></li>
        <?php } else { ?>
            <li class="nav-item"><a target="_blank" href="<?php echo base_url('Customer/MyAccount'); ?>" class="nav-link">My Account</a></li>
            <li class="nav-item"><a href="<?php echo base_url('Customer/Logout'); ?>" class="nav-link">Logout</a></li>
        <?php }?>
        <li class="nav-item cta cta-colored">
            <a target="_blank" href="Cart" class="nav-link">
                <span class="icon-shopping_cart"> </span>[<span class="" id="cart-result"><?= !empty($data['quantity']) ? $data['quantity'] : 'Empty' ?></span>]
            </a>
        </li>

    </ul>
    </div>
</div>