<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Minishop - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">

    <?php require_once('./app/views/layouts/head_script.php')?>

  </head>
   <body class="goto-here">
   
   <?php require_once('./app/views/layouts/top.php')?>

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <?php require_once('./app/views/layouts/menu.php')?>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('./public/site/images/slides/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Shop</span></p>
            <h1 class="mb-0 bread">Shop</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section bg-light">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-8 col-lg-10 order-md-last">

					<?php require_once('./app/views/pages/'. $data['manHinh'].'.php')?>

		    	</div>

		    	<div class="col-md-4 col-lg-2">
		    		<div class="sidebar">
						<div class="sidebar-box-2">
						<h2 class="heading">Categories</h2>
						<div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    
                     <?php $key = 0; ?>

                     <?php foreach ($data['categories'] as $cat) { $key ++; ?>
                     <div class="panel panel-default">
                         <div class="panel-heading" role="tab" id="heading<?php echo convertDigit($key)?>">
                             <h4 class="panel-title">
                                 <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo convertDigit($key)?>" aria-expanded="false" aria-controls="collapse<?php echo convertDigit($key)?>"><?php echo $cat->name?>
                                 </a>
                             </h4>
                         </div>
                         <div id="collapse<?php echo convertDigit($key)?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo convertDigit($key)?>">
                             <div class="panel-body">
                                <ul>
                                    <?php 
                                    $subCategory = $this->getSubCategory($cat->id);
                                    ?>
                                    <?php foreach ($subCategory as $subCate) { ?>
                                        <li><a href="#"><?php echo $subCate->name?></a></li>
                                    <?php  }?>
                                </ul>
                             </div>
                         </div>
                     </div>
                     <?php } ?>

                  </div>
               </div>
			</div>
				<div class="sidebar-box-2">
					<h2 class="heading">Price Range</h2>
					<?php require_once('./app/views/layouts/arrange_price.php')?>
				</div>
			</div>
    		</div>
    		</div>
    	</div>
	</section>
	
    <?php require_once('./app/views/layouts/footer.php')?>
    
  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <?php require_once('./app/views/layouts/foot_script.php')?>
  
    
  </body>
</html>